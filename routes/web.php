<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/registrar_articulo','articulosControlador@registrarArticulos');
Route::post('/registrar_articulo','articulosControlador@registrarArticulosPost');
Route::post('/cargar_articulos_registrados','articulosControlador@cargarArticulosRegistrados');
Route::post('/consultar_articulos','articulosControlador@consultarArticulosRegistrados');
Route::post('/consultar_articulo_especifico','articulosControlador@consultarArticuloEspecifico');
//////////////////////////////////////
Route::get('/despacho','despachoControlador@despachoGet');
Route::post('/despacho','despachoControlador@despachoPost');

Route::get('/verdespachos','despachoControlador@verDespachosGet');
