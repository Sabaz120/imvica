@extends('layouts.admin')
@section('contenido')
<div class="text-center">
  <h1>Registrar articulos</h1>
</div>
<div class="container-fluid">
  <!-- <form class="" action="index.html" method="post"> -->
    <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
    <label for="">Nombre del artículo</label>
    <input type="text" class="form-control" name="nombre_articulo" id ="nombre_articulo" value="">
    <label for="">Código de artículo</label>
    <input type="text" name="codigo_articulo" class="form-control" id="codigo_articulo" value="">
    <label for="">Categoría del artículo</label>
    <select class="form-control" id="categoriaArticulo" name="categoriaArticulo">
      @foreach($categoriasArticulos as $key)
      <option value="{{$key->id}}">{{$key->nombre}}</option>
      @endforeach
    </select>
    <label for="">Cantidad</label>
    <input type="numeric" name="cantidad" id="cantidad" class="form-control" value="">
    <label for="">Precio</label>
    <input type="text" name="precio" id="precio" class="form-control" value="">
    <br><button type="submit" class="centrar btn btn-success" onclick="registrarArticulo()" name="button">Registrar articulo</button>
  <!-- </form> -->
</div>
<hr>
<div class="container-fluid table-responsive">
  <!-- Inicio tabla de articulos registrados -->
  <table class="table table-bordered table-striped" id ="tablaArticulos">
    <thead>
      <th>Codigo de Artículo</th>
      <th>Nombre del Artículo</th>
      <th>Categoría del Artículo</th>
      <th>Cantidad</th>
      <th>Precio</th>
      <th>Acciones</th>
    </thead>
    <tbody>
      <tr>
        <td>000A</td>
        <td>Tanque de 1000 LTS</td>
        <td>Articulos de superficie</td>
        <td>100</td>
        <td>100000</td>
        <td> <button type="button" class="btn btn-success" name="button">Modificar</button> </td>
      </tr>
    </tbody>
  </table>
  <!-- Fin tabla de articulos registrados -->
</div>
@endsection
@push('scripts')
<script type="text/javascript">
  function registrarArticulo(){
    var nombre_art= $('#nombre_articulo').val()
    var codigo_art =$('#codigo_articulo').val();
    var categoria_art =$('#categoriaArticulo').val();
    var cantidad =$('#cantidad').val();
    var precio = $('#precio').val();
    var token=$('#token').val();
    // console.log(nombre_art,codigo_art,categoria_art,cantidad,precio);
    $.ajax({
      url:urlg+"/registrar_articulo",
      global: false,
      type: "POST",
      headers:{'X-CSRF-TOKEN': token},
      data:{nombre_art:nombre_art,codigo_art:codigo_art,id_categoria_articulo:categoria_art,cantidad:cantidad,precio:precio},
      dataType: "JSON",
      success: function (data) {
        if(data['success']==1){
          alert(data['mensaje']);
          $('#nombre_articulo').val("")
          $('#codigo_articulo').val("");
          $('#categoriaArticulo').val("");
          $('#cantidad').val("");
          $('#precio').val("");
          cargarTablaArticulosRegistrados()
          // location.reload();
        }else{
          alert('Se produjo un error, notifica al administrador del sistema.');
          location.reload();
        }

      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert("ERROR EN EL SERVIDOR: " + thrownError);
        return false;
      }
    });
  }
  function cargarTablaArticulosRegistrados(){
    var token=$('#token').val();
    $.ajax({
      url:urlg+"/cargar_articulos_registrados",
      global: false,
      type: "POST",
      headers:{'X-CSRF-TOKEN': token},
      data:{},
      dataType: "JSON",
      success: function (res) {
        var strHtml="";
        if(res['success']==1){
          for (var i = 0; i < res['articulos'].length; i++) {
            strHtml+='<tr>';
            strHtml+='<td>';
            strHtml+=res['articulos'][i]['codigo_articulo'];
            strHtml+='</td>';
            strHtml+='<td>';
            strHtml+=res['articulos'][i]['nombre'];
            strHtml+='</td>';
            strHtml+='<td>';
            strHtml+=res['articulos'][i]['id_categoria_articulo'];
            strHtml+='</td>';
            strHtml+='<td>';
            strHtml+=res['articulos'][i]['cantidad'];
            strHtml+='</td>';
            strHtml+='<td>';
            strHtml+=res['articulos'][i]['precio'];
            strHtml+='</td>';
            strHtml+='<td>';
            strHtml+='Boton';
            strHtml+='</td>';
            strHtml+='</tr>';
          }//for
          $('#tablaArticulos tbody').html(strHtml);
        }//success
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert("ERROR EN EL SERVIDOR: " + thrownError);
        return false;
      }
    });
  }

  $( document ).ready(function() {
    cargarTablaArticulosRegistrados()
  });

</script>
@endpush
