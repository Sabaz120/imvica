@extends('layouts.admin')
@section('contenido')
  <div class="container-fluid ">
    <div class="text-center">
      <h1>Lista de despachos  </h1>
    </div>
    <table class="table table-bordered">
      <thead>
        <th>Beneficiario</th>
        <th>Documento de identidad</th>
        <th>Fecha de despacho</th>
        <th>Acciones</th>
      </thead>
      <tbody>
        @foreach($despachos as $key)
          <tr>
            <td>{{$key->nombre_beneficiario}}</td>
            <td>{{$key->identificacion_beneficiario}}</td>
            <td>{{$key->created_at}}</td>
            <td> <a href="#" class="btn btn-success" onclick="abrirModal()">Ver detalle de despacho</a> </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="container">
            <div class="row">
              detalle
            </div> <!-- row -->
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>

    </div>
  </div>
@endsection
@push('scripts')
  <script type="text/javascript">
    function abrirModal(){
      $('#myModal').modal();
    }
  </script>
@endpush
