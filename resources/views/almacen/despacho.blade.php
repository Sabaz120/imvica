@extends('layouts.admin')
@section('contenido')
<div class="text-center">
  <h1>Despacho</h1>
</div>
<div class="container-fluid">
  <input type="hidden" id="token" name="_token" value="{{csrf_token()}}">
  <label for="">Tipo de despacho: </label>
  <select class="form-control" name="tipo_despacho" id="tipo_despacho">
    <option value="1">Venta</option>
    <option value="2">Donación</option>
  </select>
  <hr>
  <h2>Datos del beneficiario:</h2>
  <label for="">Nombre completo de persona o empresa:</label>
  <input type="text" id="nombre_beneficiario" class="form-control" value="">
  <label for="">Cédula o RIF:</label>
  <input type="text" id="documento_identificacion_beneficiario" class="form-control" value="">
  <hr>
  <h2>Articulos a despachar:</h2>
  <label for="">Seleccionar categoría de artículo</label>
  <select class="form-control" id="categoria_articulo" onchange="cargarArticulos(this.options[this.selectedIndex].value)" id="categoria_articulo">
    @foreach($categoriasArticulos as $key)
      <option value="{{$key->id}}">{{$key->nombre}}</option>
    @endforeach
  </select>
  <div id="articulo">
    <label for="" id="labelArticulo">Seleccionar artículo:</label>
    <select class="form-control" name="id_articulo" id ="id_articulo">

    </select>
  </div>
  <label for="">Cantidad del artículo</label>
  <input type="number" min="1" name="cantidad_articulo" id="cantidad_articulo" onKeyPress="return soloNumeros(event)" class="form-control" value="">
  <br>
   <div class="botonera">
     <button type="button" name="button" class="btn btn-primary " onclick="agregarArticulo()" >Agregar artículo</button>
     <button type="button" name="button" class="btn btn-success " onclick="despacharArticulos()" >Realizar despacho</button>
    </div>
  <div class="trans">

  </div>
  <hr>
  <div class="container-fluid table-responsive">
    <!-- Inicio tabla de articulos registrados -->
    <div class="text-center">
      <h3>Artículos a despachar:</h3>

    </div>
    <table class="table table-bordered table-striped" id ="tablaArticulos">
      <thead>
        <th>Nombre del Artículo</th>
        <th>Cantidad</th>
        <th>Precio Unitario</th>
        <th>Acciones</th>
      </thead>
      <tbody>
      </tbody>
    </table>
    <!-- Fin tabla de articulos registrados -->
  </div>
</div>
@push('scripts')
<script type="text/javascript">
  // Solo permite ingresar numeros.
  function soloNumeros(e){
	   var key = window.Event ? e.which : e.keyCode
	    return (key >= 48 && key <= 57)
  }
  $(document).on('click','#borrarArticulo',function(){
    // procesar_pago_balance();
    console.log('asdadadadsadisjdoajdñaodjsaodiasodijasoñdsauodaijdñoadj');
    var idArticulo=$(this).data("id");
    console.log('articulo: '+idArticulo);
    let borrar=[idArticulo];
    for(var i=0;i<articulos.length;i++){
      if(articulos[i][0]==borrar[0]){
        // delete arreglo[i];
        articulos.splice(i,1)//borra la posicion i y corre 1 puesto
      }//si articulo en la posicion 0 = al q se desea borrar
    }//for recorrer el arreglo
    console.log(articulos);
    /*
      ** Limpiar contenido de tabla
      **Recargar tabla para mostrar nuevos articulos
    */
    var body=$('#tablaArticulos tbody').html("")//limpia la tabla
    for(var i=0;i<articulos.length;i++){
      actualizarTablaArticulos(articulos[i][0],articulos[i][1]);
    }//for vuelve a llenar la tabla de articulos
  });//FIn borrarArticulo
  $( "#articulo" ).hide();
  var articulos=[];
  /* Eliminar de un arreglo unidimensional
  let forDeletion = [2, 3, 5]
  let arr = [1, 2, 3, 4, 5, 3]
  arr.push(6);
  arr = arr.filter(item => !forDeletion.includes(item))
  // !!! Read below about array.includes(...) support !!!
  console.log(arr)
  // console.info(arr)
  // [ 1, 4 ]
  */
  $( document ).ready(function() {
    cargarArticulos($('#categoria_articulo').val());
    $( "#articulo" ).show();
  });

  function agregarArticulo(){
    var articulo=$('#id_articulo').val();
    var cantidad= $('#cantidad_articulo').val();
    var bandera=0;
    if(articulo=='0')
      alert('Debe seleccionar un artículo')
    else if(cantidad=="" || cantidad==0)
      alert('Debe colocar una cantidad válida')
    else{
      for(var i=0;i<articulos.length;i++){
        if(articulos[i][0]==articulo){
          bandera=1;
          break;
        }
      }//for verificar si el articulo a agregar ya existe en la lista
      if(bandera!=1){
        articulos.push([articulo,cantidad]);//Agregar datos como arreglo bidimensional
        $('#cantidad_articulo').val("");
        $('#id_articulo option[value=0]').attr('selected','selected');
        actualizarTablaArticulos(articulo,cantidad);
        console.log(articulos);
      }
      else
        alert('Este artículo ya se encuentra agregado en la lista.');
    }//else
  }//agregarArticulo

  function actualizarTablaArticulos(id_articulo,cantidad){
    var token=$('#token').val();
    $.ajax({
      url:urlg+"/consultar_articulo_especifico",
      global: false,
      type: "POST",
      headers:{'X-CSRF-TOKEN': token},
      data:{id_articulo:id_articulo},
      dataType: "JSON",
      success: function (res) {
        if(res['success']==1){
          var body=$('#tablaArticulos tbody').html()
          body+='<tr>';
          body+='<td>';
          body+=res['articulos']['nombre'];
          body+='</td>';
          body+='<td>';
          body+=cantidad;
          body+='</td>';
          body+='<td>';
          body+=res['articulos']['precio'];
          body+='</td>';
          body+='<td>';
          body+='<button type="button" name="button" data-id='+res['articulos']['id']+' class="btn btn-danger" id="borrarArticulo"  >Descartar artículo</button>';
          body+='</td>';
          body+='</tr>';
          console.log('BOdy: '+body);
          $('#tablaArticulos tbody').html(body);
        }//success
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert("ERROR EN EL SERVIDOR: " + thrownError);
        return false;
      }
    });
  }

  function cargarArticulos(id_categoria){
    console.log(id_categoria);
    var token=$('#token').val();
    // console.log(nombre_art,codigo_art,categoria_art,cantidad,precio);
    $.ajax({
      url:urlg+"/consultar_articulos",
      global: false,
      type: "POST",
      headers:{'X-CSRF-TOKEN': token},
      data:{id_categoria_articulo:id_categoria},
      dataType: "JSON",
      success: function (data) {
        if(data['success']==1){
          // console.log(data['articulos']);
          document.getElementById("id_articulo").options.length= 0; // bora todos los option de un select
          var x = document.getElementById("id_articulo");
          var option = document.createElement("option");
          option.text = "Selecciona un artículo";
          option.value=0;
          x.add(option);
          for(var i =0;i<data['articulos'].length;i++){
            var x = document.getElementById("id_articulo");
            var option = document.createElement("option");
            option.text = data['articulos'][i]['nombre'];
            option.value=data['articulos'][i]['id'];
            x.add(option);
          }
        }else{
          alert('Se produjo un error, notifica al administrador del sistema.');
          location.reload();
        }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert("ERROR EN EL SERVIDOR: " + thrownError);
        return false;
      }
    });
  }

  function despacharArticulos(){
    var token=$('#token').val();
    var tipo_despacho=$('#tipo_despacho').val();
    var nombre_beneficiario=$('#nombre_beneficiario').val();
    var documento_identificacion_beneficiario=$('#documento_identificacion_beneficiario').val();
    $.ajax({
      url:urlg+"/despacho",
      global: false,
      type: "POST",
      headers:{'X-CSRF-TOKEN': token},
      data:{articulos:articulos,tipo_despacho:tipo_despacho,nombre_beneficiario:nombre_beneficiario,identificacion_beneficiario:documento_identificacion_beneficiario},
      dataType: "JSON",
      success: function (data) {
        console.log('despacho: '+data);
        if(data['success']==1){
          alert('Despacho registrado exitosamente');
          location.reload();
        }

      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert("ERROR EN EL SERVIDOR: " + thrownError);
        return false;
      }
    });
  }//despacharArticulos
</script>
@endpush
@endsection
