<?php

use Illuminate\Database\Seeder;
use App\categoriasArticulos;
class categoriasArticulosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $p=new categoriasArticulos;
      $p->nombre='Material de construcción';
      $p->descripcion='cemento,bloques';
      $p->save();

      $p=new categoriasArticulos;
      $p->nombre='Artículos de hogar';
      $p->descripcion='tanques de agua, etc.';
      $p->save();

    }
}
