<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\categoriasArticulos;
use App\articulos;
use App\articulos_despacho;
use App\despachos;
use Auth;
class despachoControlador extends Controller
{
    public function despachoGet(){
     	$categoriasArticulos=categoriasArticulos::all();
    	return view('almacen.despacho',array('categoriasArticulos'=>$categoriasArticulos));
    }

    public function despachoPost(Request $request){
      $despacho=new despachos;
      $despacho->nombre_beneficiario=$request->nombre_beneficiario;
      $despacho->identificacion_beneficiario=$request->identificacion_beneficiario;
      $despacho->tipo_despacho=$request->tipo_despacho;
      $despacho->usuario_id=Auth::user()->id;
      $despacho->save();
      foreach($request->articulos as $key){
        $artDespacho=new articulos_despacho;
        $artDespacho->despacho_id=$despacho->id;
        $artDespacho->articulo_id=$key[0];
        $artDespacho->cantidad_articulo=$key[1];
        $artDespacho->save();
      }
    	return response()->json(['success'=>1]);
    }

    public function verDespachosGet(){
      $despachos=despachos::orderBy('created_at','DESC')->get();
      // dd($despachos);
      return view('almacen.verDespachos',array('despachos'=>$despachos));
    }//verDespachosGet

}
