<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\categoriasArticulos;
use App\articulos;
class articulosControlador extends Controller
{
    public function registrarArticulos(){
      $categoriasArticulos=categoriasArticulos::all();
      return view('administrador.registrarArticulos',array('categoriasArticulos'=>$categoriasArticulos));
    }

    public function registrarArticulosPost(Request $request){
      $art=new articulos;
      //data:{nombre_art:nombre_art,codigo_art:codigo_art,categoria_art:categoria_art,cantidad:cantidad,precio:precio},
      $art->nombre=$request->nombre_art;
      $art->codigo_articulo=$request->codigo_art;
      $art->cantidad=$request->cantidad;
      $art->precio=$request->precio;
      $art->id_categoria_articulo=$request->id_categoria_articulo;
      $art->save();
      return response()->json(['success'=>1,'mensaje'=>'Articulo registrado exitosamente.']);
    }

    public function cargarArticulosRegistrados(){
      $articulos=articulos::all();
      return response()->json(['success'=>1,'articulos'=>$articulos]);
    }

    public function consultarArticulosRegistrados(Request $request){
      $articulos=articulos::where('id_categoria_articulo',$request->id_categoria_articulo)->get(['nombre','id']);
      return response()->json(['success'=>1,'articulos'=>$articulos]);
    }

    public function consultarArticuloEspecifico(Request $request){
      $articulo=articulos::find($request->id_articulo);
      return response()->json(['success'=>1,'articulos'=>$articulo]);
    }
}
